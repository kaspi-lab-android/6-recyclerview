package kz.company.lesson_6.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_name.view.*
import kz.company.lesson_6.R


class NameAdapter(
    private val clickListener: (name: String) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<String>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return NameViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as NameViewHolder).bind(data[position], clickListener)
    }

    fun setItems(list: List<String>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    private class NameViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_name, parent, false)) {
        private val nameTextView = itemView.nameTextView

        fun bind(item: String, clickListener: (name: String) -> Unit) {
            nameTextView.text = item
            nameTextView.setOnClickListener {
                clickListener(item)
            }
        }
    }
}