package kz.company.lesson_6.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_dog.view.*
import kz.company.lesson_6.R
import kz.company.lesson_6.model.Animal
import kz.company.lesson_6.model.Dog

class DogViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
    RecyclerView.ViewHolder(inflater.inflate(R.layout.item_dog, parent, false)) {
    private val avatarImageView = itemView.avatarImageView
    private val nameTextView = itemView.nameTextView

    fun bind(item: Animal, clickListener: (name: Animal) -> Unit) {
        item as Dog
        nameTextView.text = item.name
        avatarImageView.setBackgroundResource(item.imageRes)

        nameTextView.setOnClickListener {
            clickListener(item)
        }
    }
}