package kz.company.lesson_6.ui

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_profile.view.*
import kz.company.lesson_6.R
import kz.company.lesson_6.model.Profile


class ProfileAdapter(
    private val clickListener: (name: Profile) -> Unit
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val data = mutableListOf<Profile>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        return ProfileViewHolder(inflater, parent)
    }

    override fun getItemCount(): Int = data.size

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        (holder as ProfileViewHolder).bind(data[position], clickListener)
    }

    fun setItems(list: List<Profile>) {
        data.clear()
        data.addAll(list)
        notifyDataSetChanged()
    }

    private class ProfileViewHolder(inflater: LayoutInflater, parent: ViewGroup) :
        RecyclerView.ViewHolder(inflater.inflate(R.layout.item_profile, parent, false)) {
        private val avatarImageView = itemView.avatarImageView
        private val nameTextView = itemView.nameTextView

        fun bind(item: Profile, clickListener: (name: Profile) -> Unit) {
            nameTextView.text = item.name
            avatarImageView.setBackgroundResource(item.imageRes)

            nameTextView.setOnClickListener {
                clickListener(item)
            }
        }
    }
}