package kz.company.lesson_6

import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_main.*
import kz.company.lesson_6.model.Cat
import kz.company.lesson_6.model.Dog
import kz.company.lesson_6.model.Profile
import kz.company.lesson_6.ui.AnimalAdapter
import kz.company.lesson_6.ui.NameAdapter
import kz.company.lesson_6.ui.ProfileAdapter

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        //setupSimpleAdapter()
        //setupProfile()
        setupAnimal()
    }

    private fun setupSimpleAdapter() {
        val nameAdapter = NameAdapter(
            clickListener = {
                Log.d("name", it)
            }
        )
        val nameManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false)

        recyclerView.apply {
            adapter = nameAdapter
            layoutManager = nameManager
        }

        val nameList = listOf("Mash", "Sasha", "Dasha")
        nameAdapter.setItems(nameList)
    }

    private fun setupProfile() {
        val profileAdapter = ProfileAdapter(
            clickListener = {
                Log.d("profile", it.name)
            }
        )
        val profileManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recyclerView.apply {
            adapter = profileAdapter
            layoutManager = profileManager
        }

        val nameList = listOf(
            Profile("Маша", R.drawable.ic_launcher_background),
            Profile("Даша", R.drawable.ic_launcher_foreground)
        )
        profileAdapter.setItems(nameList)
    }

    private fun setupAnimal() {
        val animalAdapter = AnimalAdapter(
            clickListener = {
                Log.d("cat name:", (it as? Cat)?.name)
            }
        )
        val animalManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)

        recyclerView.apply {
            adapter = animalAdapter
            layoutManager = animalManager
        }

        val animalList = listOf(
            Cat("Lola", R.drawable.ic_launcher_background),
            Dog("Tapash", R.drawable.ic_launcher_foreground),
            Cat("Lola 2", R.drawable.ic_launcher_background),
            Dog("Tapash 2", R.drawable.ic_launcher_foreground),
            Cat("Lola 3", R.drawable.ic_launcher_background),
            Dog("Tapash 3", R.drawable.ic_launcher_foreground)
        )

        animalAdapter.setItems(animalList)
    }
}
