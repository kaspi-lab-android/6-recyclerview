package kz.company.lesson_6.model

import androidx.annotation.DrawableRes

data class Dog(
    val name: String,
    @DrawableRes val imageRes: Int
): Animal